<?php
	if (!isset($user)) $user = array();
?>
<div id="userinfo">
<?php
	foreach ($user as $key => $value)
	{
		if ($key == 'email' || $key == 'blog')
		{
			$href = $value;
			$target = "_blank";
			if ($key == 'email')
			{
				$href = "mailto:$value";
				$target = '_self';
			}
			$value = '<a href="'.$href.'" target="'.$target.'">'.$value.'</a>';
		}
?>
	<div class="listrow">
		<span class="listkey"><?php echo $key; ?>:</span>
		<span class="listvalue"><?php echo $value; ?></span>
	</div>
<?php		
	}
?>
</div>