<?php
	if (!isset($stylesheets))
	{
		$stylesheets = array();
	}
	else
	{
		if (is_string($stylesheets)) $stylesheets = array($stylesheets);
		if (!is_array($stylesheets)) $stylesheets = array();
	}
	if (!isset($scripts))
	{
		$scripts = array();
	}
	else
	{
		if (is_string($scripts)) $scripts = array($scripts);
		if (!is_array($scripts)) $scripts = array();
	}
?>
<DOCTYPE html>
<html>
	<head>
		<title><?php echo $title; ?></title>
<?
	foreach($stylesheets as $stylesheet)
	{
?>
		<link rel="stylesheet" type="text/css" href="<?php echo $stylesheet; ?>" />
<?php
	}
?>
	</head>
	<body>
		<div id="mainbody">
<?php require_once('views/'.$view . '.php'); ?>
		</div>
	</body>
  	<script language="javascript" type="text/javascript" src="scripts/jquery-2.2.4.min.js"></script>
<?php
	foreach ($scripts as $script)
	{
?>
  	<script language="javascript" type="text/javascript" src="<?php echo $script; ?>"></script>
<?php
	}
?>
<html>