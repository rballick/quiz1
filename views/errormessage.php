<?php
	if (!isset($errors))
	{
		$errors = array();
	}
	else
	{
		if (is_string($errors)) $errors = array($errors);
		if (!is_array($errors)) $errors = array();
	}
?>
<div class="errormessage">
<?php
	foreach($errors as $errormessage)
	{
?>
	<p><?php echo $errormessage; ?></p>
<?php
	}
?>
</div>