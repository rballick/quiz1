<?php
class UserController 
{
/*
* index
*
* displays the index page
*
* @return void
*/
	public function index() {
      // we store all the posts in a variable
		$stylesheets = array('styles/Quiz1.css');
		$scripts = array('scripts/Quiz1.js');
		$view = 'index';
		$title = 'User Info';
		require_once('views/template.php');
    }
	
/*
* userinfo
*
* displays the user information view
*
* @return void
*/
	public function userinfo()
	{
		$vars = filter_var_array($_POST,FILTER_SANITIZE_STRING);
		if ($vars['id'] != 0)
		{
			$user = new JSONUser($vars['id']);

			if (!count(get_object_vars($user)))
			{
				$_POST['errid'] = 1;
				$vars['id'] = 0;
			}
		}
		
		if ($vars['id'] != 0)
		{
			require_once('views/userinfo.php');
		}
		else
		{
			$this->errormessage();
		}
		
	}
	
/*
* errormessage
*
* displays the errormessage view
*
* @return void
*/
	public function errormessage()
	{
		$vars = filter_var_array($_POST,FILTER_SANITIZE_STRING);
		$errids = (is_array($vars['errid'])) ? $vars['errid'] : array($vars['errid']);
		$errors = array();
		foreach ($errids as $errid)
		{
			$errors[] = Error::getErrorMessage('includes/Quiz1Errors.json',$errid);
			require_once('views/errormessage.php');
		}
	}

}
?>