 $(document).ready(function(){
/* initialize autocomplete */
	$('#username').on(
		'keyup',function(){
			autocomplete($(this),'includes/Quiz1.json');
		})
		.on(
			'blur',function(){
				$('.autocomplete').remove();
		});
	$(document.body).on('mouseenter','.autocomplete',function(){
		$('#username').off('blur');
	}).on('mouseleave','.autocomplete',function(){
		$('#username').on('blur',function(){$('.autocomplete').remove();});
	});
	$(document.body).on('mouseenter','.autocomplete .listitem',function(){
		$(this).addClass('hovered');
	}).on('mouseleave','.autocomplete .listitem', function(){
		$(this).removeClass('hovered');
	}).on('click','.autocomplete .listitem',function(){
		var id = $(this).parents('.autocomplete').attr('id').replace('_autocomplete','');
		$('#'+id).val($(this).text());
		$('.autocomplete').remove();
	});
	
/* initialize AJAX form submisstion */	
	$('#frmUsername').on('submit',function(e){
		e.preventDefault();
		$('#searchresults').css({display:'none'}).html('');
		var username = $.trim($('#username').val());
		var id = 0;
		var errid = 0;
		$.getJSON( 'includes/Quiz1.json', function(data) {
			var resultSet = $.grep(data, function (e) {
				return e.username == username;
			});
			if (resultSet.length)
			{
				id = resultSet[0].id;
			}
			else
			{
				errid = (username.length == 0) ? 1 : 2;
			}
			$.post(
				'index.php',
				{
					'controller':'UserController',
					'method':'userinfo',
					'id':id,
					'errid':errid
				},
				function(data){
					$('#searchresults').html(data).css({display:'block'});
				}
			).fail(function(){
				$.post(
					'index.php',
					{
						'controller':'UserController',
						'method':'errormessage',
						'errid':errid
					},
					function(data)
					{
						$('#searchresults').html(data).css({display:'block'});
					}
				)
			});
		}).fail(function(jqxhr, textStatus, errorstring) {
			$.post(
				'index.php',
				{
					'controller':'UserController',
					'method':'errormessage',
					'errid':errid
				},
				function(data)
				{
					$('#searchresults').html(data).css({display:'block'});
				}
			)
		});
	});
});
  
/*
* autoomplete
*
* creates div with list of matches from json file based on field input value and appends to the dom
*
* @field (object) field value is derived from
* @file (string) json file name
* @return append
*/
function autocomplete(field,file)
{
	var val = field.val();
	$('.autocomplete').remove();
	if (val.length > 0)
	{
		$.getJSON( file, function(data) {
/* create autocomplete div */
			var h = field.outerHeight(true);
			var w = field.width();
			var offset = field.offset();
			var l = offset.left;
			var t = offset.top + h;
			var newDiv = $('<div/>',{
				'id': field.attr('id') + '_autocomplete',
				'class': 'autocomplete',
				'css':{'left':l+'px','top':t+'px','width':w+'px'}
			});
/* get results of the matching from beginning and add do autocomplete div */
			var resultSet = $.grep(data, function (e) {
				return e[field.attr('id')].indexOf(val) == 0;
			});
			resultSet.sort(function(obj1,obj2){
				if (obj1[field.attr('id')] < obj2[field.attr('id')]) return -1;
				if (obj1[field.attr('id')] == obj2[field.attr('id')]) return 0;
				if (obj1[field.attr('id')] > obj2) return 1;
			});
			$.each(resultSet,function(i,v){
				$('<div/>',{
					'class' : 'listitem'
				}).html(v.username.replace(val,'<b>'+val+'</b>')).appendTo(newDiv)
			});
/* get results of the matching within the string and add do autocomplete div */
			var resultSet = $.grep(data, function (e) {
				return e[field.attr('id')].indexOf(val) > 0;
			});
			resultSet.sort(function(obj1,obj2){
				if (obj1[field.attr('id')] < obj2[field.attr('id')]) return -1;
				if (obj1[field.attr('id')] == obj2[field.attr('id')]) return 0;
				if (obj1[field.attr('id')] > obj2) return 1;
			});
			$.each(resultSet,function(i,v){
				$('<div/>',{
					'class' : 'listitem'
				}).html(v.username.replace(val,'<b>'+val+'</b>')).appendTo(newDiv)
			});

			$(document.body).append(newDiv);
		});
	}
}
