<?php
	class JSONUser implements User
	{
/*
* Constructor 
*
* Gets user based on user's id
*
* @id (int) user's id
* @return void
*/
		public function __construct($id)
		{
			$userinfo = $this->getUserInfoByID($id);
			if (is_array($userinfo))
			{
				foreach ($userinfo as $key => $value)
				{
					$this->$key = $value;
				}
			}
		}
		
/*
* getUserInfoByID
*
* gets the user's information from json object based on user's id
*
* @id (int) user's id
* @return array
*/
		public function getUserInfoByID($id)
		{
			$userjson = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/test/Quiz1/includes/Quiz1.json');
			$users = json_decode($userjson,true);
			/* array_column function is new as of php5.5 */
			if (function_exists('array_column'))
			{
				$key = array_search($vars['id'], array_column($userphp, 'id'));
				if (isset($users[$key])) return $users[$key];
			}
			else
			{
				foreach ($users as $user)
				{
					if ($user['id'] == $id) return ($user);
				}
			}
			return array();
		}
	}
?>