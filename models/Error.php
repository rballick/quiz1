<?php
	class Error
	{
/*
* getErrorMessage
*
* returns array of errormessages based on errid
*
* @errorfile (string) name of json file containing error message array
* @errid (string) array key of error message
* @return string
*/		public static function getErrorMessage($errorfile,$errid)
		{
			$errorjson = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/test/Quiz1/'.$errorfile);
			$errormessages = json_decode($errorjson,true);
			if (is_array($errormessages))
			{
				return $errormessages[$errid];
			}
			return null;
		}
	}
?>