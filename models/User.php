<?php
	interface User
	{
/*
* getUserInfoByID
*
* gets the user's information based on user's id
*
* @id (int) user's id
* @return array
*/
		public function getUserInfoByID($id);
	}
?>