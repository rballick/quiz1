<?php
function autoload($className)
{
	if (is_readable("controllers/{$className}.php")) 
	{
		require_once("controllers/{$className}.php");
		return;
	}
	if (is_readable("models/{$className}.php")) 
	{
		require_once("models/{$className}.php");
		return;
	}
    if (is_readable("{$_SERVER['DOCUMENT_ROOT']}/include/classes/{$className}.php")) require_once("{$_SERVER['DOCUMENT_ROOT']}/include/classes/{$className}.php");
}
spl_autoload_register('autoload');

?>