<?php
/*
* call
*
* calls selected method from selected controller
*
* @classname (string) name of controller to be instantiated
* @method (string) name of method to be called
* @return void
*/
function call($classname, $method) 
{
	$controller = new $classname();
	$controller->{ $method }();
}

call($controller, $method);
?>